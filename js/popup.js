$(function() {
    var currentURL = '';
    var sessionStatus = false;
    var idSession = '';
        
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {type:'getPartyInfos'});
    });

    /* On écoute les messages */
    chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
        if (message.type === 'retrievePartyInfos') {
            /* A l'ouverture du popup, on verifie l'url, si elle a une session, on verifie qu'elle est valide */
            sessionStatus = message.sessionStatus;
            currentURL = new URL(message.url);
            idSession = currentURL.searchParams.get('psdIdSession');

            if(!sessionStatus) {
                if(idSession) {
                    console.log('tente de connecter...');
                    chrome.tabs.sendMessage(sender.tab.id, {type:'connectToParty', session:idSession});
                } else {
                    chrome.pageAction.onClicked.addListener(function(tab) {
                        chrome.pageAction.getPopup({tabId: tab.id});
                    });
                    console.log('Pas de session.');
                }
            } else {
                $('#url-session').val(currentURL+'?psdIdSession='+message.session);
                $('#dsp-create').hide();
                $('#activeSession').show();
            }
        } else if (message.type === 'resultConnectionParty') {
            // Réponse de la tentative de connexion
            sessionStatus = message.status;
            if(sessionStatus) {
                // la connexion a été faite donc on montre l'url a partager
                console.log('Authentifié.');
                $('#url-session').val(currentURL+'?psdIdSession='+message.session);
                $('#dsp-create').hide();
                $('#activeSession').show();
            } else {
                console.log('Non authentifié.');
            }
        } else if (message.type === 'displayError') {
            $('#dsp-create').show();
            $('#activeSession').hide();
            $('#errorMsg').replaceWith('<span class="has-text-danger is-size-6 has-text-weight-semibold	">'+message.errorMsg+'</span>');
            $('#errorMsg').show();
        } else if (message.type === 'setShareURL') {
            $('#url-session').val(currentURL+'?psdIdSession='+message.session);
            $('#dsp-create').hide();
            $('#activeSession').show();
            $('#errorMsg').hide();
        }
    });

    $('#dsp-create').click(function() { // crée la party
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {type:'createParty'});
        });
    });

    $('#btn-copy').click(function() {
        let $temp = $("<input>");
        $("body").append($temp);
        $temp.val($('#url-session').val()).select();
        document.execCommand("copy");
        $('#url-session').select();
        $temp.remove();
    });

    $('#dsp-popup-close').click(function() {
        window.close();
    });
});