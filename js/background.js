console.log('init...');
var urlChanged = false;

chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
    if(message.type === 'askMyTabId') {
        chrome.tabs.sendMessage(sender.tab.id, {type:'askedTabId', idTab: sender.tab.id});
    } else if (message.type === 'startPopup') {
        chrome.pageAction.setPopup({tabId: message.tabId, popup: 'template/popup_activated.html'});
        chrome.pageAction.show(message.tabId);    
    } else if (message.type === 'disablePopup') {
        chrome.pageAction.hide(sender.tab.id);
    }
});

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if(changeInfo.status === 'loading') { // On recupère l'URL si elle change au chargement...
        if(changeInfo.hasOwnProperty('url')) {
            urlChanged = true;
        }
    } else if (changeInfo.status === 'complete') {
        if(urlChanged) {
            chrome.tabs.sendMessage(tabId, {type:'urlChangeDetected', idTab:tabId});
            urlChanged = false;
        }
    }
});