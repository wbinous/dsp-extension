$(function() {
    var triggeredURL = window.location.href; // garde l'URL de la page
    var partyActive = false; // va permettre de savoir si la session est en cours
    var currentTabId = 0;
    var socket = io('https://seasontech.fr:3000');
    var user = {avatar: 'un.png'};
    var lastMsgUserId;
    var typing = false;
    var TYPING_TIMER_LENGTH = 400; // ms
    var COLORS = [
        '#e21400', '#91580f', '#f8a700', '#f78b00',
        '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
        '#3b88eb', '#3824aa', '#a700ff', '#d300e7',
        '#ff33cc', '#73e600', '#996633', '#669999', // perso
        '#9933ff', '#336699', '#e6e600', '#ff6600',
        '#e60000', '#0080ff', '#ff9933', '#33cc33',
    ];
    var pageTitle = document.title;
    var nbMsg = 0;
    
    user.session = new URL(triggeredURL).searchParams.get('psdIdSession');
    user.video = triggeredURL.match(/\/video\/(.*?)(?:(?!\?psdIdSession?).)*/)[0].replace('/video/','');
    chrome.storage.sync.get(['username'], function(result) { user.name = result.username; });
    chrome.storage.sync.get(['avatar'], function(result) { if(result.avatar) user.avatar = result.avatar; });

    chrome.runtime.sendMessage({type:'askMyTabId'});

    chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
        if (message.type === 'askedTabId') {
            currentTabId = message.idTab;
            chrome.runtime.sendMessage({type:'startPopup', tabId:currentTabId});
        } else if (message.type === 'createParty') { // On fait rejoidre l'utilisateur
            socket.emit('add user', user.name, user.avatar, user.video,  undefined);
        } else if (message.type === 'getPartyInfos') {
            chrome.runtime.sendMessage({type:'retrievePartyInfos', url: triggeredURL, sessionStatus: partyActive, session:user.session});
        } else if (message.type === 'connectToParty') { // On vérifie qu'une room existe dans l'URL
            if(message.session) {
                socket.emit('add user', user.name, user.avatar, user.video, undefined, message.session);
                user.session = message.session;
            } else {
                chrome.runtime.sendMessage({type:'resultConnectionParty', status:partyActive, session:user.session});
                console.log('Non authentifié.');
                partyActive = false;
            }
            return true;
        } else if (message.type === 'urlChangeDetected') { // Un changement d'URL a été detecté
            if (message.idTab == currentTabId) {
                $('#disneyshare-chat').remove();
                triggeredURL = window.location.href;
                let regex = /www.disneyplus.com\/\S*\/video\/\S*/;
                if(regex.test(triggeredURL)) { // Si l'utilisateur est sur une url de video disney+, on montre le bouton, sinon on le grise
                    partyActive = false;
                    chrome.runtime.sendMessage({type:'startPopup', tabId:currentTabId});
                } else {
                    partyActive = false;
                    chrome.runtime.sendMessage({type:'disablePopup', tabId:currentTabId});
                }
            }
        }
    });

    // Connect l'utilisateur et affiche le chat
    function displayChat() {
        partyActive = true;
        chrome.runtime.sendMessage({type:'resultConnectionParty', status:partyActive, session:user.session});
        console.log('Authentifié.');
        $.get(chrome.runtime.getURL("template/chat.html"), function(data) {
            $($.parseHTML(data)).appendTo('#hudson-wrapper');
            $('#username').val(user.name);
            $('#user-avatar').attr("src", chrome.runtime.getURL("img/avatar/"+user.avatar));
            
            let images = [ // liste des avatars
                'un.png',
                'deux.png',
                'trois.png',
                'quatre.png',
                'cinq.png',
                'six.png',
                'sept.png',
                'huit.png',
                'neuf.png'
            ];

            // On envoie tous les avatars
            for(let uneImage in images) {
                let number = parseInt(uneImage)+1;
                if (images[uneImage] === user.avatar) {
                    $('#avatar-menu').append('<option id="avatar'+number+'" data-img-src="'+chrome.runtime.getURL("img/avatar/"+images[uneImage])+'" data-img-alt="Avatar '+number+'" value="'+images[uneImage]+'" selected>Avatar '+number+'</option>');
                } else {
                    $('#avatar-menu').append('<option id="avatar'+number+'" data-img-src="'+chrome.runtime.getURL("img/avatar/"+images[uneImage])+'" data-img-alt="Avatar '+number+'" value="'+images[uneImage]+'">Avatar '+number+'</option>');
                }
            }

            $("select").imagepicker({
                hide_select : false,
                show_label  : false
            });
            if(user.name !== undefined) {
                $('#user-profile').attr("src", chrome.runtime.getURL("img/avatar/"+user.avatar));
                $('#user-settings').hide();
                $('#chat-section').show();
                $('#dsp-profile').show();
            }
        });

        document.addEventListener('visibilitychange', function(e) {
            if(!document.hidden) {
                document.title = pageTitle;
            }
        });
    }

    $('body').on('click', '#loginChat', function() {
        if($('#username').val().replace(/\s/g, "")) {
            chrome.storage.sync.set({username: $('#username').val(), avatar:$('.image-picker').first().val()});
            user.name = $('#username').val();
            user.avatar = $('.image-picker').first().val();
            $('#user-profile').attr("src", chrome.runtime.getURL("img/avatar/"+user.avatar));
            $('#username').val(user.name);
            socket.emit('update user', user.name, user.avatar);
            $('#username').removeClass('is-danger');
            $('#username-error').hide();
            $('#user-settings').hide();
            $('#chat-section').show();
            $('#dsp-profile').show();
        } else {
            $('#username').addClass('is-danger');
            $('#username-error').show();
        }
    });

    $('body').on('click', '.image_picker_selector li', function() {
        $('#user-avatar').attr("src", chrome.runtime.getURL("img/avatar/"+$('.image-picker').first().val()));
    });

    $('body').on('click', '#user-profile', function() {
        $('#user-settings').show();
        $('#chat-section').hide();
        $('#dsp-profile').hide();
    });

    $('body').on('keypress', '#inputSendMsg', function(e) {
        if (e.which == 13) {
            socket.emit('new message', $('#inputSendMsg').val());
            $('#inputSendMsg').val('');
        }

        if (partyActive) {
            if (!typing) {
                typing = true;
                socket.emit('typing');
            }
            lastTypingTime = (new Date()).getTime();

            setTimeout(() => {
                var typingTimer = (new Date()).getTime();
                var timeDiff = typingTimer - lastTypingTime;
                if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
                    socket.emit('stop typing');
                    typing = false;
                }
            }, TYPING_TIMER_LENGTH);
        }
    });

    /* functions */

    // function that display chat logs
    function displaylog(text) {
        $('#chat-container').append('<div class="media"><div class="chat-log has-text-weight-light has-text-centered is-size-7"><p class="is-italic has-text-grey">'+text+'</p></div></div>');
        lastMsgUserId = undefined;
        $('#chat-container').scrollTop($('#chat-container')[0].scrollHeight); // faire scroller au dernier message reçu
        if(document.hidden) {
            nbMsg++;
            document.title = '('+nbMsg+') '+pageTitle;
        }
    }

        // Gets the color of a username through our hash function
        const getUsernameColor = (username) => {
            // Compute hash code
            var hash = 12;
            for (var i = 0; i < username.length; i++) {
                hash = username.charCodeAt(i) + (hash << 5) - hash;
            }
            // Calculate color
            var index = Math.abs(hash % COLORS.length);
            return COLORS[index];
        }
    

    /* Server events */

    socket.on('login', (data) => {
        if(!partyActive) {
            user.session = data.session;
            chrome.runtime.sendMessage({type:'setShareURL', session:user.session});
            displayChat();
            partyActive = true;
        }
    });

    socket.on('user joined', (data) => {
        if(data.username) {
            displaylog(data.username+" joined the party.");
        } else {
            displaylog("Someone joined the party.");
        }
    });

    socket.on('user left', (data) => {
        console.log(JSON.stringify(data));
        displaylog(data.username+" left the party.");
    });

    socket.on('typing', (data) => {
        $('#is-typing').css('visibility', 'visible');
    });

    socket.on('stop typing', (data) => {
        $('#is-typing').css('visibility', 'hidden');
    });

    socket.on('connexion refused', (data) => {
        chrome.runtime.sendMessage({type:'displayError', errorMsg:data.message});
    });

    socket.on('new message', (data) => {
        if (lastMsgUserId !== undefined && lastMsgUserId === data.idUser) {
            $('.content').last().append('<p>'+data.message+'</p>');
        } else {
            if(socket.id === data.idUser) {
                $('#chat-container').append('<div class="media"><div class="media-content"><div class="content"><p class="has-text-right"><strong style="color:'+getUsernameColor(data.username)+';">'+data.username+'</strong></p><p>'+data.message+'</p></div></div><figure class="media-right image is-48x48"><img class="is-rounded" src="'+chrome.runtime.getURL("img/avatar/"+data.avatar)+'" id="user-avatar" /><span class="dateMsg">'+(new Date(data.date)).toLocaleTimeString()+'</span></figure></div>');
            } else {
                $('#chat-container').append('<div class="media"><figure class="media-left image is-48x48"><img class="is-rounded" src="'+chrome.runtime.getURL("img/avatar/"+data.avatar)+'" id="user-avatar" /><span class="dateMsg">'+(new Date(data.date)).toLocaleTimeString()+'</span></figure><div class="media-content"><div class="content"><p class="has-text-left"><strong style="color:'+getUsernameColor(data.username)+';">'+data.username+'</strong></p><p>'+data.message+'</p></div></div></div>');
            }
        }
        lastMsgUserId = data.idUser;
        $('#chat-container').scrollTop($('#chat-container')[0].scrollHeight); // faire scroller au dernier message reçu
        if(document.hidden) {
            nbMsg++;
            document.title = '('+nbMsg+') '+pageTitle;
        }
    });

    socket.on('disconnect', () => {
        displaylog("You have been disconnected.");
        partyActive = false;
    });

    socket.on('reconnect', () => {
        if (user.session) {
            partyActive = true;
            socket.emit('reconnectUser', user.name, user.avatar, user.video, undefined, user.session);
            displaylog('You have been reconnected.');
        }
    });

    socket.on('reconnect_error', () => {
        displaylog('Attempt to reconnect has failed.');
    });
});